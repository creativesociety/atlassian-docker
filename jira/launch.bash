#!/bin/bash
set -o errexit

. /usr/local/share/atlassian/common.bash

sudo own-volume
rm -f /opt/atlassian-home/.jira-home.lock

if [ "$CONTEXT_PATH" == "ROOT" -o -z "$CONTEXT_PATH" ]; then
  CONTEXT_PATH=
else
  CONTEXT_PATH="/$CONTEXT_PATH"
fi

xmlstarlet ed -u '//Context/@path' -v "$CONTEXT_PATH" conf/server-backup.xml > conf/server.xml

if [ -n "$DATABASE_URL" ]; then
  extract_database_url "$DATABASE_URL" DB /opt/jira/lib
  DB_JDBC_URL="$(xmlstarlet esc "$DB_JDBC_URL")"
  SCHEMA=''
  if [ "$DB_TYPE" != "mysql" ]; then
    SCHEMA='<schema-name>public</schema-name>'
  fi

  cat <<END > /opt/atlassian-home/dbconfig.xml
<?xml version="1.0" encoding="UTF-8"?>
<jira-database-config>
  <name>defaultDS</name>
  <delegator-name>default</delegator-name>
  <database-type>$DB_TYPE</database-type>
  $SCHEMA
  <jdbc-datasource>
    <url>$DB_JDBC_URL</url>
    <driver-class>$DB_JDBC_DRIVER</driver-class>
    <username>$DB_USER</username>
    <password>$DB_PASSWORD</password>
    <pool-min-size>20</pool-min-size>
    <pool-max-size>20</pool-max-size>
    <pool-max-wait>30000</pool-max-wait>
    <pool-max-idle>20</pool-max-idle>
    <pool-remove-abandoned>true</pool-remove-abandoned>
    <pool-remove-abandoned-timeout>300</pool-remove-abandoned-timeout>
  </jdbc-datasource>
</jira-database-config>
END
fi

if [ -n "$PROXY_URL" ]; then
  xmlstarlet ed -P -S -L -s /Server/Service -t elem -n ConnectorTMP -v "" \
  -i //ConnectorTMP -t attr -n "acceptCount" -v "100" \
  -i //ConnectorTMP -t attr -n "connectionTimeout" -v "20000" \
  -i //ConnectorTMP -t attr -n "disableUploadTimeout" -v "true" \
  -i //ConnectorTMP -t attr -n "enableLookups" -v "false" \
  -i //ConnectorTMP -t attr -n "maxHttpHeaderSize" -v "8192" \
  -i //ConnectorTMP -t attr -n "maxThreads" -v "150" \
  -i //ConnectorTMP -t attr -n "minSpareThreads" -v "8080" \
  -i //ConnectorTMP -t attr -n "protocol" -v "HTTP/1.1" \
  -i //ConnectorTMP -t attr -n "redirectPort" -v "8443" \
  -i //ConnectorTMP -t attr -n "useBodyEncodingForURI" -v "true" \
  -i //ConnectorTMP -t attr -n "scheme" -v "https" \
  -i //ConnectorTMP -t attr -n "proxyName" -v "$PROXY_URL_ARRAY" \
  -i //ConnectorTMP -t attr -n "proxyPort" -v "443" \
  -i //ConnectorTMP -t attr -n "secure" -v "true" \
  -i //ConnectorTMP -t attr -n "port" -v "9090" \
  -r //ConnectorTMP -v Connector \
conf/server.xml
fi

/opt/jira/bin/start-jira.sh -fg
